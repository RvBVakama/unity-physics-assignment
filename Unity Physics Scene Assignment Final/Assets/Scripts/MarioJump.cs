﻿//--------------------------------------------------------------------------------------
// Class for MarioJump
// learned from this source https://forum.unity.com/threads/mario-style-jumping.381906/
//--------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//--------------------------------------------------------------------------------------
// MarioJump object
// If the player jumps they jump, if they hold jump they go higher.
//--------------------------------------------------------------------------------------
public class MarioJump : MonoBehaviour
{
    // the force at which the player jumps
    public float fJumpForce;
    // max jump time
    private float fJumpDuration;
    // saving the jump duration so we can revert
    public float fJumpTime;

    // is the player touching the ground? (eligible to jump)
    public bool bGrounded;
    // what the ground is
    public LayerMask lyrmskGround;
    // when the player lets go of the jump key and results in stopping the jump
    private bool bStoppedJumping = false;

    // transform for the feet, checks if the player is touching the ground
    public Transform transTouchingGround;
    // is the players feet near the ground layer?
    private float fGroundCheckRadius = 0.1f;

    // players rigidbody
    private Rigidbody rb;

    //--------------------------------------------------------------------------------------
    // Use this for initialization.
    //--------------------------------------------------------------------------------------
    void Start()
    {
        // assigning the rigidbody
        rb = GetComponent<Rigidbody>();
        // saving the jump duration so we can revert
        fJumpDuration = fJumpTime;
    }

    //--------------------------------------------------------------------------------------
    // Update is called once per frame.
    //--------------------------------------------------------------------------------------
    void Update()
    {
        // put a sphere around the feets transform and check if the sphere (with the radius that is passed in)
        // is overlapping with any layer which is set to the ground, assign to bool
        bGrounded = Physics.CheckSphere(transTouchingGround.position, fGroundCheckRadius, lyrmskGround);

        // if the player touched the ground reset jump duration
        if (bGrounded)
        {
            fJumpDuration = fJumpTime;
            rb.drag = 5;
        }
    }

    //--------------------------------------------------------------------------------------
    // FixedUpdate is used for updating physics.
    //--------------------------------------------------------------------------------------
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) && bGrounded)
        {
            // change the rigidbodys y velocity to be the jump force, resulting in a jump
            rb.velocity = new Vector3(rb.velocity.x, fJumpForce, rb.velocity.z);
            // player is jumping so make sure the bool is set to false
            bStoppedJumping = false;
        }

        // if the player is still holding space and is still jumping continue to apply the jump force
        if (Input.GetKey(KeyCode.Space) && !bStoppedJumping && fJumpDuration > 0)
        {
            rb.velocity = new Vector3(rb.velocity.x, fJumpForce, rb.velocity.z);
            fJumpDuration -= Time.deltaTime;
        }

        // if the player lets go of jump then the character stop gaining upward velocity
        if (Input.GetKeyUp(KeyCode.Space))
        {
            fJumpDuration = 0;
            bStoppedJumping = true;
        }

        // if they are falling and not on the ground
        if (!bGrounded && rb.velocity.y < 0)
        {
            rb.drag = 1;
        }
    }
}

