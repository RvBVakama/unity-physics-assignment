﻿//--------------------------------------------------------------------------------------
// Class for the MouseLook
//--------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//--------------------------------------------------------------------------------------
// MouseLook object
// Used to rotate the camera based on the mouses movement in the application.
//--------------------------------------------------------------------------------------
public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 100.0f;
    public float clampAngle = 80.0f;
    
    // rotation on the X axis
    private float fRotX = 0.0f;
    // rotation on the Y axis
    private float fRotY = 0.0f; 

    //--------------------------------------------------------------------------------------
    // Use this for initialization.
    //--------------------------------------------------------------------------------------
    void Start()
    {
        // set a vector 3 rot to rotation of the local euler angles, aka the rotation of 
        // the camera
        Vector3 rot = transform.localRotation.eulerAngles;
        fRotY = rot.y;
        fRotX = rot.x;

        // lock the mouse in the game
        Cursor.lockState = CursorLockMode.Locked;
    }

    //--------------------------------------------------------------------------------------
    // Update is called once per frame and is used to manage mouse lock state, and rotate
    // the camera based on the mouses movement in the application.
    //--------------------------------------------------------------------------------------
    void Update()
    {
        // lock to the centre and make the mouse invisible ingame when the mouse button is clicked
        if (Input.GetMouseButtonDown(0))
            Cursor.lockState = CursorLockMode.Locked;
        // eelease mouse
        if (Input.GetKeyDown(KeyCode.Return))
            Cursor.lockState = CursorLockMode.None;

        // unitys mouse x and mouse y input axis
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        // based on the mouse sensitivity and movement in the application over deltatime
        // add it to the axis rotation floats
        fRotY += mouseX * mouseSensitivity * Time.deltaTime;
        fRotX += mouseY * mouseSensitivity * Time.deltaTime;

        // clamp the position of the range of the rotation of the camera
        fRotX = Mathf.Clamp(fRotX, -clampAngle, clampAngle);

        // converting to quaternion so we can use it in the transform of the object
        Quaternion localRotation = Quaternion.Euler(fRotX, fRotY, 0.0f);
        transform.rotation = localRotation;
    }
}

