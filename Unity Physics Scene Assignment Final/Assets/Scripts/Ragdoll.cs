﻿//--------------------------------------------------------------------------------------
// Class for the Ragdoll
//--------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//--------------------------------------------------------------------------------------
// Ragdoll object
// Makes the player ragdoll by manipulating its valeus that simulate ragdoll mode.
// Requires an animator.
//--------------------------------------------------------------------------------------
[RequireComponent(typeof(Animator))]
public class Ragdoll : MonoBehaviour
{
    // assigning the objects components
    private Animator animator = null;
    // list of all rigidbody objects on the ragdoll, aka the body parts
    public List<Rigidbody> rigidbodies = new List<Rigidbody>();
    // ragdoll player script
    private RagdollPlayer scpRagdollPlayer;
    // assign bool true or false based on if it has an animator or not and if it does 
    // assign all the rigidbodys to 'r' and make them all kinematic true or false 
    public bool RagdollOn
    {
        get
        {
            return !animator.enabled;
        }

        set
        {
            animator.enabled = !value; foreach (Rigidbody r in rigidbodies) r.isKinematic = !value;
        }
    }

    //--------------------------------------------------------------------------------------
    // Use this for initialization.
    //--------------------------------------------------------------------------------------
    void Start()
    {
        scpRagdollPlayer = GetComponent<RagdollPlayer>();
        animator = GetComponent<Animator>();
        // setting all rigidbodys to kinematic true
        foreach (Rigidbody r in rigidbodies) r.isKinematic = true;
    }

    //--------------------------------------------------------------------------------------
    // Update is called once per frame and is used to check if RagdollOn is enabled or not
    // so it can tell the ragdoll player script to go ragdoll mode.
    //--------------------------------------------------------------------------------------
    void Update()
    {
        if (RagdollOn)
            scpRagdollPlayer.enabled = false;
    }
}

