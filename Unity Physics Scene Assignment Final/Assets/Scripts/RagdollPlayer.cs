﻿//--------------------------------------------------------------------------------------
// Class for the RagdollPlayer
//--------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//--------------------------------------------------------------------------------------
// RagdollPlayer object
// The ragdoll player that can be controlled with the arrow keys, walks fast when it 
// tries to walk through the portal it activates the ragdoll properties.
// Requires a character controller and animator.
//--------------------------------------------------------------------------------------
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class RagdollPlayer : MonoBehaviour
{
    // initializing the objects components
    CharacterController controller = null;
    Animator animator = null;

    // initializing some variables
    public float speed = 80.0f;
    public float pushPower = 2.0f;

    //--------------------------------------------------------------------------------------
    // Use this for initialization.
    //--------------------------------------------------------------------------------------
    void Start()
    {
        // assigning the components
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    //--------------------------------------------------------------------------------------
    // Update is called once per frame and is used to check input for movement.
    //--------------------------------------------------------------------------------------
    void Update()
    {
        // getting the unity input controls from movement on the vertical and horizontal axis
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        // how the player moves about
        controller.SimpleMove(transform.up * Time.deltaTime);
        transform.Rotate(transform.up, horizontal * speed * Time.deltaTime);
        // the reason the player animates at the speed
        animator.SetFloat("Speed", vertical * speed * Time.deltaTime);
    }

    //--------------------------------------------------------------------------------------
    // When the player hits something it is pushed with semi realistic force.
    // 
    // Param:
    //      hit: The object that the player collided with.
    //--------------------------------------------------------------------------------------
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        // the rigidbody on the object that was hit
        Rigidbody body = hit.collider.attachedRigidbody;
        // if it is not meant to move dont attempt to
        if (body == null || body.isKinematic)
            return;
        
        // if the player was falling faster than a certain speed dont move it
        if (hit.moveDirection.y < -0.3F)
            return;

        // push the object hit with some force
        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
        body.velocity = pushDir * pushPower;
    }
}
