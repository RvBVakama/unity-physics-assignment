﻿//--------------------------------------------------------------------------------------
// Class for the RagdollTrigger
//--------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//--------------------------------------------------------------------------------------
// RagdollTrigger object
// Place on the player so when they touch a certain tag trigger it activates ragdoll.
//--------------------------------------------------------------------------------------
public class RagdollTrigger : MonoBehaviour
{
    // the ragdoll object
    private Ragdoll ragdoll;

    //--------------------------------------------------------------------------------------
    // The ragdoll triggers when it touches the correct tag.
    //
    // Param:
    //      other: The object it collided with.
    //--------------------------------------------------------------------------------------
    void OnTriggerEnter(Collider other)
    {
        // the ragdoll object
        ragdoll = GetComponentInParent<Ragdoll>();

        // if the touched object has the right tag activate ragdoll mode
        if (other.tag == "ActivateRagdoll")
            ragdoll.RagdollOn = true;
    }
}