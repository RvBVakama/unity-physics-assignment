﻿//--------------------------------------------------------------------------------------
// Class for the RigidBodyPlayer
//--------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//--------------------------------------------------------------------------------------
// RigidBodyPlayer object
// The character that the player moves around in the scene and uses to interact with the
// world. Contains logic for movement, crouching, sprinting, climbing slopes and stairs.
//--------------------------------------------------------------------------------------
public class RigidBodyPlayer : MonoBehaviour
{
    // the players rigidbody
    private Rigidbody rb;
    // public movement speed float
    public float fMoveSpeed = 1;
    // storing the original move speed so we can revert
    private float fOrigMoveSpeed;
    // accessing the mouse script
    private MouseLook scpMouseLook;
    // accessing the transform of the head child
    private Transform transHead;
    // remembering the heads previous position
    private Vector3 v3HeadlastPos;
    // accessing the transform of the body child
    private Transform transBody;
    // the mario jump script
    public MarioJump scpMarioJump;
    // reduce the speed of movement when falling
    private float fReduceSpeedPercent = 1;
    // the object that ray is hitting currently
    private RaycastTarget scpHoverTarget;
    // if the ray touched a specific object
    private bool bHovered = false;
    // check to see if the ray has touched any applicable objects yet
    private bool bRayTouched = false;
    // if the player is crouched or not 
    private bool bCrouched = false;
    // if the player can stand up from being crouched
    private bool bCanStand = true;
    // user let go of crouch when standing was not allowed
    private bool bUserLetGoOfCrouchSoStandUp = false;
    // slope ray cast transform
    private Transform transSlope;
    // slope checker ray cast transform
    private Transform transCheckerSlope;
    // slope character position ray cast transform
    private Transform transCharacterPosSlope;
    // the power the character will push objects at
    public float pushPower = 1.0f;

    //--------------------------------------------------------------------------------------
    // Use this for initialization.
    //--------------------------------------------------------------------------------------
    void Start()
    {
        // assigning the rigidbody component
        rb = GetComponent<Rigidbody>();
        scpMouseLook = transform.GetChild(0).transform.GetComponent<MouseLook>();

        // saveing the original move speed
        fOrigMoveSpeed = fMoveSpeed;

        // body parts transforms
        transHead = transform.GetChild(0).GetComponent<Transform>();
        transBody = transform;

        // initializing the slope raycast transform
        transSlope = new GameObject().transform;
        // initializing the slope checker raycast transform
        transCheckerSlope = new GameObject().transform;
        // initializing the character position raycast transform
        transCharacterPosSlope = new GameObject().transform;
    }

    //--------------------------------------------------------------------------------------
    // Update is called once per frame and is used to check input for player movement, 
    // crouch, sprinting etc...
    //--------------------------------------------------------------------------------------
    void Update()
    {
        // movement controls
        // decreasing speed if the player is airborne
        if (!scpMarioJump.bGrounded && rb.velocity.y < 0)
            fReduceSpeedPercent = 15;
        // setting back to normal speed
        else
            fReduceSpeedPercent = 1;
        // moving the player via its rigidbody forward with physics at the defined move speed
        if (Input.GetKey(KeyCode.W))
            rb.AddForce(transform.forward * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // moving the player via its rigidbody backwards with physics at the defined move speed
        if (Input.GetKey(KeyCode.S))
            rb.AddForce(-transform.forward * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // moving the player via its rigidbody left with physics at the defined move speed
        if (Input.GetKey(KeyCode.A))
            rb.AddForce(-transform.right * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // moving the player via its rigidbody right with physics at the defined move speed
        if (Input.GetKey(KeyCode.D))
            rb.AddForce(transform.right * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // setting the heads position to the bodys position simulating a crouch
        if (Input.GetKey(KeyCode.LeftControl))
        {
            transHead.transform.position = transBody.transform.position;
            bCrouched = true;
        }
        // the user let go of crouch whilst standing was not allowed, let them stand up
        if (Input.GetKeyUp(KeyCode.LeftControl) && !bCanStand && bCrouched)
            bUserLetGoOfCrouchSoStandUp = true;
        // returning the head to its last position uncrouching
        if (Input.GetKeyUp(KeyCode.LeftControl) && bCanStand || bUserLetGoOfCrouchSoStandUp && bCanStand)
        {
            v3HeadlastPos.x = transHead.position.x;
            v3HeadlastPos.z = transHead.position.z;
            v3HeadlastPos.y = transBody.position.y + ((transBody.GetComponent<SphereCollider>().radius * 2) - 0.036f);
            transHead.position = v3HeadlastPos;
            bCrouched = false;
            bUserLetGoOfCrouchSoStandUp = false;
        }
        // go faster when holding shift
        if (Input.GetKey(KeyCode.LeftShift))
        {
            fMoveSpeed = fOrigMoveSpeed + (fOrigMoveSpeed / 2);
        }
        // return to normal speed when you let go fo shift
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            fMoveSpeed = fOrigMoveSpeed;
        }
        // shoots a ray out from the players view
        if (Input.GetMouseButtonDown(0))
        {
            Ray selectorRay = new Ray(transHead.position, transHead.forward);
            RaycastHit selectorHit;
            // shoots a ray from the players perspective
            if (Physics.Raycast(selectorRay, out selectorHit))
            {
                // enter statement if the ray hit an object of type RaycastTarget
                if (selectorHit.transform.GetComponent<RaycastTarget>())
                {
                    // tell the object that the ray hit it
                    RaycastTarget scpTarget = selectorHit.transform.GetComponent<RaycastTarget>();
                    scpTarget.bHitByRay = true;
                }
            }
        }

        // cant stand from a crouch if there is not enough head room
        Ray standRay = new Ray(transHead.position, Vector3.up);
        RaycastHit standHit;
        // throw a sphere with the same radius of the players sphere body upwards for infinity distance
        if (Physics.SphereCast(standRay, transHead.GetComponent<SphereCollider>().radius * 0.9f, out standHit, Mathf.Infinity) && bCrouched)
        {
            // if there is enough room to stand up then set the stand bool to true
            if (standHit.distance > transHead.GetComponent<SphereCollider>().radius * 3)
                bCanStand = true;
            // if you cant stand tell the bool you cant
            else
                bCanStand = false;
        }
        // if the sphere projection does not hit anything above and you cant stand then you can actually stand
        // aka you were somewhere you couldn't stand but you walked to somewhere you can and now you will
        else if (!bCanStand)
            bCanStand = true;

        // climb slopes and climb slopes
        transSlope.position = new Vector3((transBody.forward.x * GetComponent<SphereCollider>().radius * 1.1f) + transBody.position.x, transBody.position.y, (transBody.forward.z * GetComponent<SphereCollider>().radius * 1.1f) + transBody.position.z);
        transCheckerSlope.position = new Vector3((transBody.forward.x * GetComponent<SphereCollider>().radius * 1.01f) + transBody.position.x, transBody.position.y, (transBody.forward.z * GetComponent<SphereCollider>().radius * 1.01f) + transBody.position.z);
        transCharacterPosSlope.position = transBody.position;
        // a ray shot downwards positioned infront of the player
        Ray slopeRay = new Ray(transSlope.position, -transBody.up);
        // a ray shot downwards positioned a little closer to infront of the player
        Ray slopeCheckerRay = new Ray(transCheckerSlope.position, -transBody.up);
        // a ray shot downwards from the players position
        Ray slopeCharacterPosRay = new Ray(transCharacterPosSlope.position, -transBody.up);
        RaycastHit slopeHit;
        RaycastHit slopeCheckerHit;
        RaycastHit slopeCharacterPosHit;
        // if the player not jumping and is on the ground
        if (rb.velocity.y <= 0.01f && scpMarioJump.bGrounded || rb.velocity.y >= 0.01f && scpMarioJump.bGrounded)
        {
            // if the player is moving or holding w
            if (rb.velocity.x > 0.01f && rb.velocity.z > 0.01f || Input.GetKey(KeyCode.W))
            {
                if (Physics.Raycast(slopeRay, out slopeHit))
                {
                    Physics.Raycast(slopeCheckerRay, out slopeCheckerHit);
                    Physics.Raycast(slopeCharacterPosRay, out slopeCharacterPosHit);

                    // if the object you are trying to walk at are stairs and you want to go down them
                    if (slopeHit.point.y == slopeCheckerHit.point.y && slopeCharacterPosHit.point.y > slopeCheckerHit.point.y && slopeCheckerHit.transform.tag != "Ground")
                        transBody.position = new Vector3(transBody.position.x, slopeCharacterPosHit.point.y + GetComponent<SphereCollider>().radius, transBody.position.z);
                    // if you want to climb a slope
                    else if (slopeHit.point.y > slopeCheckerHit.point.y && slopeCheckerHit.transform.tag != "Ground")
                        transBody.position = new Vector3(transBody.position.x, slopeCheckerHit.point.y + GetComponent<SphereCollider>().radius * 1.1f, transBody.position.z);
                    // if you want to climb some stairs
                    else if (slopeHit.point.y == slopeCheckerHit.point.y && slopeCheckerHit.transform.tag != "Ground")
                        transBody.position = new Vector3(transBody.position.x, slopeCheckerHit.point.y + GetComponent<SphereCollider>().radius * 1.1f, transBody.position.z);
                }
            }
        }

        // raycast shootable object select colour changer logic
        Ray hoverRay = new Ray(transHead.position, transHead.forward);
        RaycastHit hoverHit;
        if (Physics.Raycast(hoverRay, out hoverHit))
        {
            // if you hit an object with the class RaycastTarget on it
            if (hoverHit.transform.GetComponent<RaycastTarget>())
            {
                // tell the object it was hit by a ray (that classes bool) and that it was touched by a ray (this classes bool, prevents nasty null exceptions)
                scpHoverTarget = hoverHit.transform.GetComponent<RaycastTarget>();
                scpHoverTarget.bHoverHitByRay = true;
                bRayTouched = true;
            }
            // if the ray has touched something at all (to prevent null exceptions)
            else
                bHovered = true;

            // if the ray has touched something
            if (bHovered && bRayTouched)
            {
                scpHoverTarget.bHoverHitByRay = false;
                bHovered = false;
            }
        }

        // rotating the player with the camera on the y axis by zeroing the other axis
        Quaternion rot = scpMouseLook.transform.rotation;
        rot.x = 0;
        rot.z = 0;
        transform.rotation = rot;

        // if the player hits r the game restarts
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
    }

    //--------------------------------------------------------------------------------------
    // When the player hits something it is pushed with semi realistic force.
    // 
    // Param:
    //      hit: The object that the player collided with.
    //--------------------------------------------------------------------------------------
    void OnCollisionEnter(Collision hit)
    {
        // the rigidbody on the object that was hit
        Rigidbody body = hit.collider.attachedRigidbody;
        
        // if it is not meant to move dont attempt to
        if (body == null || body.isKinematic)
            return;
        
        // push the object hit with some force
        Vector3 pushDir = new Vector3(hit.relativeVelocity.x, 0, hit.relativeVelocity.z);
        body.velocity = -pushDir * pushPower;
    }
}

