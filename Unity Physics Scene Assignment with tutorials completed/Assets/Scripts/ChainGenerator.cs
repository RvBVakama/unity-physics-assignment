﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainGenerator : MonoBehaviour
{
    public float fLength = 0;
    public GameObject goChainLink;
    public HingeJoint hinge3D;

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < fLength; i++)
        {
            GameObject go = Instantiate(goChainLink);
            Vector3 vec3 = transform.position;
            vec3 = new Vector3(transform.position.x, transform.position.y - 0.09f * i, transform.position.z);
            go.transform.position = vec3;
            go.transform.Rotate(Vector3.up, 90.0f * i);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
