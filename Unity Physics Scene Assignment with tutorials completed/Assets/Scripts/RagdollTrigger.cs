﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollTrigger : MonoBehaviour
{
    private Ragdoll ragdoll;

    void OnTriggerEnter(Collider other)
    {
        ragdoll = GetComponentInParent<Ragdoll>();

        if (other.tag == "ActivateRagdoll")
            ragdoll.RagdollOn = true;
    }
}