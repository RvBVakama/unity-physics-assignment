﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastTarget : MonoBehaviour
{
    private Rigidbody rb = null;
    private MeshRenderer mr = null;
    private Color color;
    public Material selectMat;
    private Color selectColour;
    [HideInInspector]
    public bool bHitByRay = false;
    [HideInInspector]
    public bool bHoverHitByRay = false;
    public bool bExemptFromGravityChange;

    // Use this for initialization
    void Start()
    {
        // assigns the rigidbody if the object has one and then sets the 
        // gravity to false and inematic to true rendereing the object static
        if (rb = transform.GetComponent<Rigidbody>())
        {
            rb.useGravity = false;
            rb.isKinematic = true;
        }

        // assigns the mesh renderer if the object has one
        if (mr = transform.GetComponent<MeshRenderer>()) { }

        // getting the select colour from the material
        selectColour = selectMat.color;

        // saving the material so we can revert
        color = mr.material.color;

        // enables gravity if the object has the bool set to true
        if (bExemptFromGravityChange)
        {
            rb.useGravity = true;
            rb.isKinematic = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // if rigidbody has been assigned
        if (rb)
        {
            // if hit by a the players raycast
            if (bHitByRay)
            {
                rb.useGravity = true;
                rb.isKinematic = false;
            }
            // if mesh renderer has been assigned
            if (mr)
            {
                // only highlight if gravity is off (object has not be activated)
                if (!rb.useGravity)
                {
                    // change the objects colour to show that it is a raycastable object
                    if (bHoverHitByRay)
                        mr.material.color = selectColour;
                    // revert the object to is original colour
                    else
                        mr.material.color = color;
                }
                // revert the object to is original colour after its 
                // activated to ensure it stays its non selected colour
                else
                    mr.material.color = color;
            }
        }
    }
}

