﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidBodyPlayer : MonoBehaviour
{
    // the players rigidbody
    private Rigidbody rb;
    // public movement speed float
    public float fMoveSpeed = 1;
    // storing the original move speed so we can revert
    private float fOrigMoveSpeed;
    // accessing the mouse script
    private MouseLook scpMouseLook;
    // accessing the transform of the head child
    private Transform transHead;
    // remembering the heads previous position
    private Vector3 v3HeadlastPos;
    // accessing the transform of the body child
    private Transform transBody;
    // the mario jump script
    public MarioJump scpMarioJump;
    // reduce the speed of movement when falling
    private float fReduceSpeedPercent = 1;
    // the object that ray is hitting currently
    private RaycastTarget scpHoverTarget;
    // if the ray touched a specific object
    private bool bHovered = false;
    // check to see if the ray has touched any applicable objects yet
    private bool bRayTouched = false;
    // if the player is crouched or not 
    private bool bCrouched = false;
    // if the player can stand up from being crouched
    private bool bCanStand = true;
    // user let go of crouch when standing was not allowed
    private bool bUserLetGoOfCrouchSoStandUp = false;
    // slope ray cast transform
    private Transform transSlope;
    // slope checker ray cast transform
    private Transform transCheckerSlope;
    // slope character position ray cast transform
    private Transform transCharacterPosSlope;

    // Use this for initialization
    void Start()
    {
        // lock to the centre and make the mouse invisible ingame
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        scpMouseLook = transform.GetChild(0).transform.GetComponent<MouseLook>();

        // saveing the original move speed
        fOrigMoveSpeed = fMoveSpeed;

        // body parts transforms
        transHead = transform.GetChild(0).GetComponent<Transform>();
        transBody = transform;

        // initializing the slope raycast transform
        transSlope = new GameObject().transform;
        transCheckerSlope = new GameObject().transform;
        transCharacterPosSlope = new GameObject().transform;
    }

    // Update is called once per frame
    void Update()
    {
        // movement controls
        if (!scpMarioJump.bGrounded && rb.velocity.y < 0)
            fReduceSpeedPercent = 15;
        else
            fReduceSpeedPercent = 1;
        // moving the player via its rigidbody forward with physics at the defined move speed
        if (Input.GetKey(KeyCode.W))
            rb.AddForce(transform.forward * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // moving the player via its rigidbody backwards with physics at the defined move speed
        if (Input.GetKey(KeyCode.S))
            rb.AddForce(-transform.forward * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // moving the player via its rigidbody left with physics at the defined move speed
        if (Input.GetKey(KeyCode.A))
            rb.AddForce(-transform.right * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // moving the player via its rigidbody right with physics at the defined move speed
        if (Input.GetKey(KeyCode.D))
            rb.AddForce(transform.right * fMoveSpeed / fReduceSpeedPercent, ForceMode.Acceleration);
        // setting the heads position to the bodys position simulating a crouch
        if (Input.GetKey(KeyCode.LeftControl))
        {
            transHead.transform.position = transBody.transform.position;
            bCrouched = true;
        }
        // the user let go of crouch whilst standing was not allowed, let them stand up
        if (Input.GetKeyUp(KeyCode.LeftControl) && !bCanStand && bCrouched)
            bUserLetGoOfCrouchSoStandUp = true;
        // returning the head to its last position uncrouching
        if (Input.GetKeyUp(KeyCode.LeftControl) && bCanStand || bUserLetGoOfCrouchSoStandUp && bCanStand)
        {
            v3HeadlastPos.x = transHead.position.x;
            v3HeadlastPos.z = transHead.position.z;
            v3HeadlastPos.y = transBody.position.y + ((transBody.GetComponent<SphereCollider>().radius * 2) - 0.036f);
            transHead.position = v3HeadlastPos;
            bCrouched = false;
            bUserLetGoOfCrouchSoStandUp = false;
        }
        // go faster when holding shift
        if (Input.GetKey(KeyCode.LeftShift))
        {
            fMoveSpeed = fOrigMoveSpeed + (fOrigMoveSpeed / 2);
        }
        // return to normal speed when you let go fo shift
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            fMoveSpeed = fOrigMoveSpeed;
        }
        // shoots a ray out from the players view
        if (Input.GetMouseButtonDown(0))
        {
            Ray selectorRay = new Ray(transHead.position, transHead.forward);
            RaycastHit selectorHit;
            if (Physics.Raycast(selectorRay, out selectorHit))
            {
                if (selectorHit.transform.GetComponent<RaycastTarget>())
                {
                    RaycastTarget scpTarget = selectorHit.transform.GetComponent<RaycastTarget>();
                    scpTarget.bHitByRay = true;
                }
            }
        }

        // cant stand from a crouch if there is not enough head room
        Ray standRay = new Ray(transHead.position, Vector3.up);
        RaycastHit standHit;
        if (Physics.SphereCast(standRay, transHead.GetComponent<SphereCollider>().radius * 0.9f, out standHit, Mathf.Infinity) && bCrouched)
        {
            if (standHit.distance > transHead.GetComponent<SphereCollider>().radius * 3)
                bCanStand = true;
            else
                bCanStand = false;
        }
        else if (!bCanStand)
            bCanStand = true;

        // climb slopes & climb slopes
        transSlope.position = new Vector3((transBody.forward.x * GetComponent<SphereCollider>().radius * 1.1f) + transBody.position.x, transBody.position.y, (transBody.forward.z * GetComponent<SphereCollider>().radius * 1.1f) + transBody.position.z);
        transCheckerSlope.position = new Vector3((transBody.forward.x * GetComponent<SphereCollider>().radius * 1.01f) + transBody.position.x, transBody.position.y, (transBody.forward.z * GetComponent<SphereCollider>().radius * 1.01f) + transBody.position.z);
        transCharacterPosSlope.position = transBody.position;
        Ray slopeRay = new Ray(transSlope.position, -transBody.up);
        Ray slopeCheckerRay = new Ray(transCheckerSlope.position, -transBody.up);
        Ray slopeCharacterPosRay = new Ray(transCharacterPosSlope.position, -transBody.up);
        RaycastHit slopeHit;
        RaycastHit slopeCheckerHit;
        RaycastHit slopeCharacterPosHit;
        if (rb.velocity.y <= 0.01f && scpMarioJump.bGrounded || rb.velocity.y >= 0.01f && scpMarioJump.bGrounded)
        {
            if (rb.velocity.x > 0.01f && rb.velocity.z > 0.01f || Input.GetKey(KeyCode.W))
            {
                if (Physics.Raycast(slopeRay, out slopeHit))
                {
                    Physics.Raycast(slopeCheckerRay, out slopeCheckerHit);
                    Physics.Raycast(slopeCharacterPosRay, out slopeCharacterPosHit);
                    // stairs and slopes
                    if (slopeHit.point.y == slopeCheckerHit.point.y && slopeCharacterPosHit.point.y > slopeCheckerHit.point.y && slopeCheckerHit.transform.tag != "Ground")
                    {
                        transBody.position = new Vector3(transBody.position.x, slopeCharacterPosHit.point.y + GetComponent<SphereCollider>().radius, transBody.position.z);
                    }
                    else if (slopeHit.point.y > slopeCheckerHit.point.y && slopeCheckerHit.transform.tag != "Ground")
                    {
                        transBody.position = new Vector3(transBody.position.x, slopeCheckerHit.point.y + GetComponent<SphereCollider>().radius * 1.1f, transBody.position.z);
                    }
                    else if (slopeHit.point.y == slopeCheckerHit.point.y && slopeCheckerHit.transform.tag != "Ground")
                    {
                        transBody.position = new Vector3(transBody.position.x, slopeCheckerHit.point.y + GetComponent<SphereCollider>().radius * 1.1f, transBody.position.z);
                    }
                }
            }
        }

        // raycast shootable object select colour changer logic
        Ray hoverRay = new Ray(transHead.position, transHead.forward);
        RaycastHit hoverHit;
        if (Physics.Raycast(hoverRay, out hoverHit))
        {
            if (hoverHit.transform.GetComponent<RaycastTarget>())
            {
                scpHoverTarget = hoverHit.transform.GetComponent<RaycastTarget>();
                scpHoverTarget.bHoverHitByRay = true;
                bRayTouched = true;
            }
            else
                bHovered = true;

            if (bHovered && bRayTouched)
            {
                scpHoverTarget.bHoverHitByRay = false;
                bHovered = false;
            }
        }

        // rotating the player with the camera on the y axis by zeroing the other axis
        Quaternion rot = scpMouseLook.transform.rotation;
        rot.x = 0;
        rot.z = 0;
        transform.rotation = rot;
    }
}

